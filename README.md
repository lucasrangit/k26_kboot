# README #

Port of the Kinetis Bootloader 1.2.0 KBOOT flash resident bootloader to K26.

KBOOT allows for in-field programming of Kinetic MCUs using the same tools (and protocols) as their factory flash loaders over USB, I2C, SPI, etc... This port focuses only on the USB interface however.

# References

1. http://www.nxp.com/products/microcontrollers-and-processors/arm-processors/kinetis-cortex-m/kinetis-symbols-footprints-and-models/kinetis-bootloader:KBOOT
1. https://www.nxp.com/webapp/Download?colCode=FSL_KINETIS_BOOTLOADER_1_2_0&appType=license&location=null&Parent_nodeId=1395762639877723974876&Parent_pageType=product